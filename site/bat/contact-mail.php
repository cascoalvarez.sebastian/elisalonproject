<?php
/**
 * Created by IntelliJ IDEA.
 * User: casco
 * Date: 5/5/2020
 * Time: 5:47 PM
 */

    // Only process POST reqeusts.
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Get the form fields and remove whitespace.
        $name = strip_tags(trim($_POST["name"]));
        $secname = trim($_POST["sec-name"]);
				/*$name = str_replace(array("\r","\n"),array(" "," "),$name);*/
        $email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);
        $phone = trim($_POST["phone"]);
        $message = trim($_POST["message"]);

        // Check that data was sent to the mailer.
        if ( empty($name) OR empty($message) OR empty($phone) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // Set a 400 (bad request) response code and exit.
            http_response_code(400);
            die('MF003');
            exit;
        }

        // Set the recipient email address.
        // FIXME: Update this to your desired email address.
        $recipient = "elisbeautyinfo@gmail.com";

        // Set the email subject.
        $subject = "New contact from $name";

        // Build the email content.
        $email_content = "Name: $name $secname\n";
        $email_content .= "Email: $email\n\n";
        $email_content .= "Phone: $phone\n\n";
        $email_content .= "Message:\n$message\n";

        // Build the email headers.
        $email_headers = "From: $name <$email>";

        // Send the email.
        if (mail($recipient, $subject, $email_content, $email_headers)) {
            // Set a 200 (okay) response code.
            http_response_code(200);
            die('MF000');

        } else {
            // Set a 500 (internal server error) response code.
            http_response_code(500);
            die('MF255');
        }

    } else {
        // Not a POST request, set a 403 (forbidden) response code.
        http_response_code(403);
        die('MF255');
    }
